//
//  SearchCategoryCollectionViewCell.swift
//  BeerCrafts
//
//  Created by Sabaz on 22/03/20.
//  Copyright © 2020 Sabaz. All rights reserved.


import UIKit

class SearchCategoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var bgViewOut: UIView!
    @IBOutlet weak var iconImgOut: UIImageView!
    @IBOutlet weak var nameLblOut: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
