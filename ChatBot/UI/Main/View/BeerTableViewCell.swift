//
//  BeerTableViewCell.swift
//  BeerCrafts
//
//  Created by Sabaz on 22/03/20.
//  Copyright © 2020 Sabaz. All rights reserved.
//

import UIKit

protocol BeerTableViewCellDelegate: class {
    func didTapButton(mainIndxPath: IndexPath?, subIndxPath: IndexPath)
}

class BeerTableViewCell: TableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var collection: UICollectionView!
    
    var dataList:[String] = ["vew", "vsdvsd"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collection.register(UINib(nibName: "SearchCategoryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SearchCategoryCollectionViewCell")
        collection.delegate = self
        collection.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func configure(_ item: Any?) {
        guard let model = item as? Beer else { return }
        
        let txtArray = model.details.text.components(separatedBy: "|")
        self.nameLabel.text =  txtArray.joined(separator: "\n")
        if model.details.id == "#answer#" {
            self.nameLabel.textAlignment = .right
            self.nameLabel.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        }
        else {
            self.nameLabel.textAlignment = .left
            self.nameLabel.font = UIFont.systemFont(ofSize: 17, weight: .regular)
        }
        
        if model.isRead || model.details.tag == "bye" {
            collection.isHidden = true
        }
        else {
            collection.isHidden = false
        }
        
        
        switch model.details.replies {
        case .string(let str):
            dataList = [str]
            
        case .strArray(let strArry):
            dataList = strArry
        default:
            break
        }
        collection.reloadData()
    }
}


//MARK:- CollectionView Datasource and Delegate
extension BeerTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellHeight = collectionView.frame.size.height
        var catText = dataList[indexPath.row]
        let lblSizeWidth = (catText as NSString).size(withAttributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 15)]).width
        let newCellWidth = lblSizeWidth + 15
        
        if newCellWidth >= 20 { // image width 36
            return CGSize(width: newCellWidth, height: CGFloat(integerLiteral: Int(cellHeight)))
        }
        else {
            return CGSize(width: 20, height: CGFloat(integerLiteral: Int(cellHeight)))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let ro = indexPath.row
        let cell : SearchCategoryCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchCategoryCollectionViewCell", for: indexPath) as! SearchCategoryCollectionViewCell
        
        cell.bgViewOut.cornerRadius = 3
        cell.nameLblOut.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        let cat = self.dataList[ro]
        cell.nameLblOut.text = cat
        cell.nameLblOut.textColor = .white
        cell.bgViewOut.backgroundColor = UIColor.black//UIColor(hex: "b6b7b9")
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        
        (self.delegate as? BeerTableViewCellDelegate)?.didTapButton(mainIndxPath: indxPath, subIndxPath: indexPath)
    }
}
