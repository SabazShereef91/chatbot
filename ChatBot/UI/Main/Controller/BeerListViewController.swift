//
//  BeerListViewController.swift
//  BeerCrafts
//
//  Created by Sabaz on 22/03/20.
//  Copyright © 2020 Sabaz. All rights reserved.


import UIKit

class BeerListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    private var viewModel = BeerListViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
        self.setupViewModel()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
        
    @IBAction func sortAction(_ sender: Any) {
        //self.viewModel.sortItems()
    }
}

extension BeerListViewController {
    
    private func setupTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "BeerTableViewCell", bundle: nil), forCellReuseIdentifier: "BeerTableViewCell")
    }
    
    private func setupViewModel() {
        self.viewModel.reloadHandler = {
            self.tableView.reloadData()
        }
        
        self.showLoader()
        self.viewModel.fetchItems { _ in
            self.hideLoader()
        }
    }
}

extension BeerListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.itemCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell( withIdentifier: "BeerTableViewCell" ) as! BeerTableViewCell
        cell.delegate = self
        cell.indxPath = indexPath
        
        cell.item = self.viewModel.item(indexPath)
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension BeerListViewController: BeerTableViewCellDelegate {
    func didTapButton(mainIndxPath: IndexPath?, subIndxPath: IndexPath) {
                
        guard let mIndx = mainIndxPath else {
            return
        }
        self.viewModel.updateModels(mIndx, subIndxPath)
       
    }
    
    
    
    
}

