//
//  MainViewController.swift
//  BeerCrafts
//
//  Created by Sabaz on 22/03/20.
//  Copyright © 2020 Sabaz. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    private struct Segue {
        static let BeerList = "BeerListVCSegueId"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func beerListAction(_ sender: Any) {
        self.performSegue(withIdentifier: Segue.BeerList, sender: nil)
    }
}

