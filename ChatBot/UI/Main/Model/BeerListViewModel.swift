//
//  BeerListViewModel.swift
//  BeerCrafts
//
//  Created by Sabaz on 22/03/20.
//  Copyright © 2020 Sabaz. All rights reserved.
//

import Foundation
import UIKit

typealias DataHandler = () -> Void

class BeerListViewModel {
    
    init() { }
    
    var items: [Beer] = []
    var fullItems: [Beer] = []
    var reloadHandler: DataHandler = { }
    private var isAscending: Bool = false
    
    var itemCount: Int {
        return self.items.count
    }
    
    func item(_ indexPath: IndexPath) -> Beer {
        return self.items[indexPath.row]
    }
    
    func fetchItems(completion: @escaping (_ error: Error?) -> Void) {
        getBeerListOffline { [weak self] (list, error) in
            if let error = error {
                completion(error)
            } else {
                print("TOTAL ITEMS: \(list.count)")
//                DispatchQueue.main.asyncAfter(deadline: .now() + 4, execute: {
//                    self?.configureModels(list: list)
//                })
                self?.configureModels(list: list)
                completion(nil)
            }
        }
    }
    
    private func configureModels(list: [Beer]) {
        
        self.fullItems = list
        //self.items = self.fullItems
        if let startItem = list.filter({ $0.details.tag == "\($0.details.lesson)-start"}).first {
            self.items.append(startItem)
        }
        
        
        self.reloadHandler()
    }
    
    func updateModels(_ mindexPath: IndexPath, _ sindexPath: IndexPath) {
        
        self.items[mindexPath.row].isRead = true
        let datItem = self.items[mindexPath.row]
        print(datItem)
        
        var replies:[String] = []
        var payloads:[String] = []
        var routes:[String] = []
        
        
        switch datItem.details.replies {
        case .string(let str):
            replies = [str]
        case .strArray(let strArry):
            replies = strArry
        default:
            break
        }
        
        switch datItem.details.payloads {
        case .string(let str):
            payloads = [str]
        case .strArray(let strArry):
            payloads = strArry
        default:
            break
        }
        
        switch datItem.details.routes {
        case .string(let str):
            routes = [str]
        case .strArray(let strArry):
            routes = strArry
        default:
            break
        }
        
        
        let newItem = Beer(name: "", isRead: true, details: Beer.Details(id: "#answer#",
                                                                              text: replies[sindexPath.row],
                                                                              tag: "",
                                                                              lesson: "",
                                                                              replies: MetadataType.string(""),
                                                                              payloads: MetadataType.string(payloads[sindexPath.row]),
                                                                              routes: MetadataType.string("")))
        
        self.items.append(newItem)
        
        if let newI = self.fullItems.filter({$0.name == routes[sindexPath.row]}).first {
            self.items.append(newI)
        }
        
        self.reloadHandler()
    }
    
}

// FILTER
extension BeerListViewModel {
    
  
    func clearFilters() {
        self.resetData()
    }
}

// SEARCH
extension BeerListViewModel {
    
    func filterSearch(searchText: String) {
    }
    
    func resetData() {
        self.reloadHandler()
    }
}

// SORT
extension BeerListViewModel {
    
       func getBeerListOffline(completion: @escaping (_ beerList: [Beer], _ error: Error?) -> Void) {
            
            guard let filePath = Bundle.main.url(forResource: "allornothing",
                                                 withExtension: "json") else {
                completion([], AppError.fileNotFound)
                return
            }

            do {
                
                    
                let data = try Data.init(contentsOf: filePath)
                let json = try JSONDecoder().decode([String: Beer.Details].self, from: data)
                var pairs: [Beer] = []
                for (name, details) in json {
                    let pair = Beer(name: name, details: details)
                    pairs.append(pair)
                }
                print(pairs)

                
                
    //            let data = try Data.init(contentsOf: filePath)
    //            let json = try JSON(data: data)
                let list: [Beer] = []//try json.arrayValue.map { try Beer.parse($0) }
                completion(pairs, nil)
            } catch {
                completion([], error)
            }
        }    
}


enum MetadataType: Codable {
  case strArray([String])
  case string(String)

  init(from decoder: Decoder) throws {
    let container = try decoder.singleValueContainer()
    do {
      self = try .strArray(container.decode([String].self))
    } catch DecodingError.typeMismatch {
      do {
        self = try .string(container.decode(String.self))
      } catch DecodingError.typeMismatch {
        throw DecodingError.typeMismatch(MetadataType.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Encoded payload not of an expected type"))
      }
    }
  }

  func encode(to encoder: Encoder) throws {
    var container = encoder.singleValueContainer()
    switch self {
    case .strArray(let strArray):
      try container.encode(strArray)
    case .string(let string):
      try container.encode(string)
    }
  }
}

struct Beer {
    let name: String
    var isRead: Bool = false
    let details: Details
    
    struct Details: Codable {
        let id, text,tag, lesson: String
        let replies, payloads, routes : MetadataType?
    }
}


enum AppError: Error {
    
    case fileNotFound
    case custom(message: String)
    
    var ErrorMessage: String {
        switch self {
        case .fileNotFound:
            return "File Not Found"
        case .custom(let message):
            return message
        }
    }
}
