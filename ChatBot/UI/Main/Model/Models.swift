//
//  Models.swift
//  BeerCrafts
//
//  Created by Sabaz on 22/03/20.
//  Copyright © 2020 Sabaz. All rights reserved.
//

import Foundation



class SectionModel {
    
    var headerModel: Any?
    var cellModels: [Any] = []
    var footerModel: Any?
    
    init(headerModel: Any? = nil,
         cellModels: [Any] = [],
         footerModel: Any? = nil) {
        self.headerModel = headerModel
        self.cellModels = cellModels
        self.footerModel = footerModel
    }
}

class FilterCellModel {
    
    var value: String
    var isSelected: Bool
    
    init(value: String,
         isSelected: Bool = false) {
        self.value = value
        self.isSelected = isSelected
    }
}

