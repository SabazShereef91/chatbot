//
//  TableViewCell.swift
//  BeerCrafts
//
//  Created by Sabaz on 22/03/20.
//  Copyright © 2020 Sabaz. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    var indxPath: IndexPath?
    var item: Any? {
        didSet {
            self.configure(self.item)
        }
    }
    
    weak var delegate: NSObjectProtocol? = nil
    
    func configure(_ item: Any?) { }
}

