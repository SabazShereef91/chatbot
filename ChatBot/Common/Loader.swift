//
//  Loader.swift
//  BeerCrafts
//
//  Created by Sabaz on 22/03/20.
//  Copyright © 2020 Sabaz. All rights reserved.
//

import UIKit

class Loader {
    
    static func showAdded(to view: UIView, animated: Bool){
       // MBProgressHUD.showAdded(to: view, animated: animated)
    }
    
    static func hide(for view: UIView, animated: Bool){
       // MBProgressHUD.hide(for: view, animated: animated)
    }
}

extension UIViewController {
    
    func showLoader(animated: Bool = false) {
        Loader.showAdded(to: self.view, animated: animated)
    }
    
    func hideLoader(animated: Bool = false) {
        Loader.hide(for: self.view, animated: animated)
    }
}
